# @summary Install and configure DirectMairie
#
# Install and configure DirectMairie, the open source citizen participation tool for cities streets
#
# @example
#     class { 'directmairie':
#       smtp_host        => 'smtp.example.org',
#       smtp_port        => 587,
#       smtp_user_name   => 'KateBush@example.org',
#       smtp_user_passwd => 'RunningUpThatHill!',
#       app_dir          => '/home/directmairie/DirectMairie',
#       picture_dir      => '/home/directmairie/DirectMairie-pictures',
#       security_key     => '0123654789abcdefghijklm',
#       ci_job_name      => 'myJob',
#       db_user_password => 'S3cr#TPassw0rdz',
#     }
#
# @param smtp_host Hostname of SMTP relay host
# @param smtp_port Port of SMTP relay host
# @param smtp_user_name Username for SMTP credentials
# @param smtp_user_passwd Password for SMTP credentials
# @param app_dir Directory in which application will be copied
# @param picture_dir Directory holding user-uploaded pictures (may be fat)
# @param security_key Secret key for the application (used as salt)
# @param ci_job_name Gitlab CI job name from which to download DirectMairie jar file
# @param db_user_password Password for DirectMairie database
# @param package_list List of packages to be installed beforehand (stored in Hiera)

class directmairie (
  String $smtp_host,
  Integer $smtp_port,
  String $smtp_user_name,
  String $smtp_user_passwd,
  # Directory for DirectMairie application, eg /home/directmairie/DirectMairie
  String $app_dir,
  # Directory for users uploaded pictures
  String $picture_dir,
  # Secret key (used as salt)
  String $security_key,
  # CI Job name from which downloading directmairie jar file (build artefact)
  String $ci_job_name,
  # Password for DirectMairie database
  String $db_user_password,
  Array $package_list, # Value of $package_list are specified in Hiera

  # Git refspec to download DirectMairie (can be branch, a tag, or a commit)
  String $refspec           = 'master',
  String $db_host           = 'localhost',
  Integer $db_port          = 5432,
  String $db_name           = 'directmairie',
  String $db_for_tests_name = 'directmairie_test',
  String $db_user_name      = 'directmairie',
  String $system_username   = 'directmairie',
  # Activate debug / dev mode
  Boolean $debug            = false,
) {

  # #############################################################################
  # Variables

  $_gitlab_srv = 'gitlab.adullact.net'
  $_gitlab_group = 'directmairie'
  $_gitlab_project_name = 'directmairie'

  # URL of DirectMairie source code repository
  $_directmairie_gitlab_full_url = "${_gitlab_srv}/${_gitlab_group}/${_gitlab_project_name}"
  # URL of DirectMairie jar file (grabbed from a CI job)
  # see https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html#downloading-the-latest-artifacts
  $_directmairie_jar_url = join(
    [
      "https://${_directmairie_gitlab_full_url}",
      "/-/jobs/artifacts/${refspec}/raw/backend/build/libs/amies.jar", # TODO change name once renamed in upstream
      "?job=${ci_job_name}",
    ]
  )
  # Filename of DirectMairie jar file (build artefact)
  $jar_name = "directmairie-${refspec}.jar"

  # Postgres role for DirectMairie
  $_directmairie_db_role = 'directmairie'
  # JDBC URL string
  $directmairie_jdbc_url = "jdbc:postgresql://${db_host}:${db_port}/${db_name}"

  # #############################################################################
  # PREREQUISITES packages

  package { $directmairie::package_list:
    ensure => present,
  }

  # #############################################################################
  # PREREQUISITES Postgres

  # Role creation
  postgresql::server::role { $_directmairie_db_role:
    password_hash => postgresql_password($db_user_name, $db_user_password),
  }

  # DB + Postgres user creation
  -> postgresql::server::db { $db_name:
    user     => $db_user_name,
    password => postgresql_password($db_user_name, $db_user_password),
  }

  # Privileges granting
  -> postgresql::server::database_grant { $db_name:
    privilege => 'ALL',
    db        => $db_name,
    role      => $_directmairie_db_role,
  }

  # Create DB needed for running tests
  if $directmairie::debug {
    postgresql::server::db { $db_for_tests_name:
      user     => $db_user_name,
      password => postgresql_password($db_user_name, $db_user_password),
    }
    postgresql::server::database_grant { $db_for_tests_name:
      privilege => 'ALL',
      db        => $db_for_tests_name,
      role      => $_directmairie_db_role,
      require   => Postgresql::Server::Role[$_directmairie_db_role],
    }
  }

  # #############################################################################
  # Prepare DirectMairie

  # Create directory for users uploaded pictures
  file { 'DirectMairie picture dir':
    ensure => directory,
    path   => $picture_dir,
    mode   => '0700',
    owner  => $system_username,
  }

  # Create systemd unit-file
  file { 'DirectMairie systemd unit':
    ensure  => file,
    path    => '/etc/systemd/system/directmairie.service',
    mode    => '0644',
    content => epp('directmairie/directmairie.service.epp'),
    require => Archive['download-directmairie-jar'],

  }

  # Get DirectMairie jar file
  archive { 'download-directmairie-jar':
    ensure => 'present',
    path   => "${directmairie::app_dir}/${directmairie::jar_name}",
    source => $_directmairie_jar_url,
  }

  # #############################################################################
  # Launch DirectMairie

  service { 'launch DirectMairie':
    ensure  => 'running',
    name    => 'directmairie',
    enable  => true,
    require => [
      Package[$directmairie::package_list],
      File['DirectMairie systemd unit'],
      Postgresql::Server::Database_grant[$db_name],
    ]
  }
}
