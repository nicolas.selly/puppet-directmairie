# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 2.1.0 - 2020-02-06

### Changed

* Downloaded jar file from now includes refspec/version in its name (eg `directmairie-2.1.0.jar`)

## 2.0.0 - 2019-09-24

### Changed

* Rename module from `amies` to `directmairie` #9
* Remove `ccze` package #1

## 1.0.2 - 2019-06-26

### Added

* First release 
