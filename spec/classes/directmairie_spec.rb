require 'spec_helper'

describe 'directmairie' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) do
        os_facts
      end
      let(:pre_condition) do
        'require postgresql::server'
      end
      let(:params) do
        {
          smtp_host: 'smtp.example.org',
          smtp_port: 587,
          smtp_user_name: 'katebush@example.org',
          smtp_user_passwd: 'RunningUpThatHill!',
          app_dir: '/home/directmairie/DirectMairie',
          picture_dir: '/home/directmairie/DirectMairie-pictures',
          security_key: '0123654789abcdefghijklm',
          ci_job_name: 'myJob',
          db_user_password: 'S3cr#TPassw0rdz',
        }
      end

      it {
        is_expected.to compile
      }
    end
  end
end
